# BeyondLR

This project is for CS750 Machine learning class substitute. (Chapter 7 ISLR)

Post-class [video](https://youtu.be/K-3hLZmIR00) and re-annotated [slide](https://gitlab.com/MonkieDein/beyondlr/-/blob/main/pos-blr.pdf) is available for review.

## General Flow

1. Introduce Non-linear regression (Exponential Growth of technology)
2. Check for good fit (Residual assumption and plot)
3. Polynomial Regression (Drawback and motivation for piecewise function)
4. Piecewise Function (Impose locality but has discountinuity)
    - Understand piecewise constant causes discountinuity
4. Continuous Piecewise Function (Not smooth)
    - Understand continuous piecewise linear basis term causes (corner)
    - Understand continuous piecewise quadratic basis term causes (f''(x) to be not continous)
    - If we want to have f(x),f'(x) and f''(x) then we should use polynomial + continuous piecewise polynomial (degree >= 3) for basis function. 
5. Splines
    - Higher degree of splines will have continuity in more order of deriavative.
    - Cubic splines is default (generally used).
6. Natural Splines
    - Natural splines has linear boundary.
    - Tighter confidence region, more stable.
7. Choosing Knots
    - Cross Validation is suggested for choosing (k) number of knots.
    - Uniform Quantile is suggested for choosing position of knots (c1,c2 ... ck).
8. Other techniques
    - Smoothing splines (Minimizing sum of squares criterion subject to smoothness penalty)
    - Local Regression (Region are allowed to overlap)
    - GAM General additive models (Extend methods above with multiple predictors)
    - Time series (endogenous variables)
    - Cyclic pattern (wavelet, radial, fourier)
    - Heteroskedasticity (Weighted Least Square)



